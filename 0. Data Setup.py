"""
Performs all necessary dataset preprocessing
"""
#%% Setup
import os
import numpy  as np
import pandas as pd

from tqdm import tqdm
from PIL  import Image
#%% Constants
ROOT_FOLDER = "dataset/"

#%% Utility Functions
# Bounding Box
def bbox(image):
    """
    Determines the bounding boxes for images to remove empty space where possible
    :param image:
    :return:
    """
    HEIGHT = image.shape[0]
    WIDTH  = image.shape[1]

    for i in range(image.shape[1]):
        if (image[:, i] > 0).sum() >= 1 and (image[:, i] > 0).sum() <= 255 * 20:
            x_min = i - 1 if (i > 1) else 0
            break

    for i in reversed(range(image.shape[1])):
        if (image[:, i] > 0).sum() >= 1  and (image[:, i] > 0).sum() <= 255 * 20:
            x_max = i + 2 if (i < WIDTH - 2) else WIDTH
            break

    for i in range(image.shape[0]):
        if (image[i] > 0).sum() >= 1  and (image[i] > 0).sum() <= 255 * 20:
            y_min = i - 1 if (i > 1) else 0
            break

    for i in reversed(range(image.shape[0])):
        if (image[i] > 0).sum() >= 1  and (image[i] > 0).sum() <= 255 * 20:
            y_max = i + 2 if (i < HEIGHT - 2) else HEIGHT
            break

    return x_min, y_min, x_max, y_max

# %% Dataset generation
# Loop through the datasets
for path,name in zip(["train.csv", "Dig-MNIST.csv", "test.csv"], ["train", "dig-mnist", "test"]):
    # Load the dataset
    df = pd.read_csv(ROOT_FOLDER + path)

    # Create the folders if necessary
    if not os.path.exists(ROOT_FOLDER + name):
        os.mkdir(ROOT_FOLDER + name)

    # Label and image ID storage
    ids    = []
    labels = []

    # Loop through each row
    for i, row in tqdm(df.iterrows()):
        # Get the data components
        id = "{} {}".format(name, i)
        img = np.reshape(row[1:].values, newshape=(28, 28)).astype(np.uint8)

        # Remove empty space
        x_min, y_min, x_max, y_max = bbox(img)
        img_ = img[y_min:y_max, x_min:x_max]

        # Check again
        #x_min, y_min, x_max, y_max = bbox(img)
        #img = img[y_min:y_max, x_min:x_max]

        # Pad
        img_ = np.pad(img_, (1,1), "constant", constant_values=(0,0))

        # Remove any non-maximum pixels
        img_[img_ >= 100] = 255
        img_[img_ < 100] = 0

        # Convert and reshape
        img_ = Image.fromarray(img_)
        img_ = img_.resize((28,28), Image.ANTIALIAS)

        # Fetch the label
        try:
            label = row["label"]
        except:
            label = 0

        # Write to storage
        img_.save(ROOT_FOLDER + "{}/{}.png".format(name, id))
        ids.append(id)
        labels.append(label)

        # QA
        if name == "test" and i == 7:
            a=0

    # Save the IDs to a DataFrame and write to CSV
    label_df = pd.DataFrame({"id": ids, "label": labels})
    label_df.to_csv(ROOT_FOLDER + "{} labels.csv".format(name), index=False)
