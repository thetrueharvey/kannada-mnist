"""
This first script uses K-fold methodology to train a number of networks, to ultimately be used for ensembling
Tensorboard: tensorboard --logdir lightning_logs
"""
#%% Setup

import numpy  as np
import pandas as pd

import torch
import torch.nn            as nn
import torch.nn.functional as F

from sklearn.model_selection import KFold

import classification_transforms as T
import Dataset                   as ds
import CustomNetV2
import LightningModel
import Loss
import Utils
import Logger

from torchvision      import transforms
from torchtools.optim import RangerLars
from pytorch_lightning import Trainer, Callback
from pytorch_lightning.loggers import TensorBoardLogger

from pytorch_lightning import __version__

torch.cuda.is_available()

#%% Dataset
# Train transform
train_transform = transforms.Compose([ T.ChooseOne([ #T.RandomNoise(decay=1/600e3, decay_stop=0.8, probability=0.5)
                                                     T.RandomErasures(n_erasures=[1,3], h=[0.15, 0.4], w=[0.15, 0.4],  decay=1/600e3, decay_stop=0.9, probability=1.0)
                                                   , T.RandomAffine(decay=1/600e3, decay_stop=0.5, probabilities=[0.7,0.7,0.7,0.7,0.7,0.7])
                                                   #, T.RandomCrop(decay=1/600e3, decay_stop=0.3, probability=0.5)
                                                   #, T.RandomColourJitter(decay=1/600e3, decay_stop=0.1, probabilities=[0.2,0.2,0.2,0.2,0.2])
                                                   , T.Skew(probability=1.0)
                                                   #, T.RandomDistortion(probability=1.0, grid=[5,5], magnitude=9)
                                                   ]
                                                  )
                                     , transforms.ToTensor()
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([transforms.ToTensor()])

# Load the data
df_1 = pd.read_csv("dataset/train labels.csv")
df_2 = pd.read_csv("dataset/dig-mnist labels.csv")

# Split the main dataset into folds
folds = KFold(n_splits=5, random_state=101).split(df_1)

# Create the datasets
ds_1 = [[ds.KannadaMNIST( image_folder="dataset/train"
                        , labels      =df_1.iloc[index]
                        , transforms  =train_transform
                        )
        for index in indices] for indices in folds
       ]

ds_2 = ds.KannadaMNIST( image_folder="dataset/dig-mnist"
                      , labels      =df_2
                      , transforms  =test_transform
                      )

# Create loaders
loader_1 = [[torch.utils.data.DataLoader(dataset=ds, batch_size=128, shuffle=True if i == 0 else False)
             for i,ds in enumerate(train_test)
            ]
            for train_test in ds_1
           ]
loader_2 = torch.utils.data.DataLoader(dataset=ds_2, batch_size=128, shuffle=False)

#%% LR scheduler
class ReduceLROnPlateu(Callback):
    def __init__(self, optimizer):
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau( optimizer=optimizer
                                                                   , mode="min"
                                                                   , factor=0.5
                                                                   , patience=4
                                                                   , verbose=1
                                                                   , min_lr=1e-9
                                                                   , cooldown=0
                                                                   )

    def on_epoch_end(self, trainer, pl_module):
        self.scheduler.step(pl_module.val_loss)

#%% K-fold network training
for i,(train_loader, val_loader) in enumerate(loader_1):
    net = CustomNetV2.KannadaNet( input_shape=[1, 28, 28]
                                , n_class    =10
                                )

    # Loss
    loss = Loss.CrossEntropyConfidenceLoss()

    # Optimizer
    #optimizer = RangerLars(params=net.parameters(), lr=0.0001)
    optimizer = torch.optim.RMSprop(params=net.parameters(), lr=0.0001)

    # Create a Lightning model
    model = LightningModel.KannadaMNISTModel( train_loader=train_loader
                                            , val_loader  =[val_loader, loader_2]
                                            , net         =net
                                            , loss        =loss
                                            , optimizer   =optimizer
                                            )

    # Training
    trainer = Trainer( gpus             =1
                     , gradient_clip_val=0.1
                     , logger           =TensorBoardLogger( save_dir="lightning_logs"
                                                          , name    ="model_fold_{}".format(i)
                                                          )
                     , callbacks        =[ReduceLROnPlateu(optimizer)]
                     , progress_bar_refresh_rate=1
                     , fast_dev_run             =False
                     )
    trainer.fit(model)

a=0
