"""
Uses posterior adjustment to alter model weights for final prediction
"""
#%% Setup
from pickle import dump, load
import numpy  as np
import pandas as pd

import torch

import classification_transforms as T
import Dataset                   as ds
import MobileNetV2
import CustomNet
import CustomNetV2
import Loss
import Utils
import Logger

from PosteriorAdjustment import posterior_adjustment

from torchvision      import transforms
from torchtools.optim import RangerLars

torch.cuda.is_available()

#%% Dataset
# Transform
transform = transforms.Compose([transforms.ToTensor()])

# Load the data
df_1 = pd.read_csv("dataset/train labels.csv")
df_2 = pd.read_csv("dataset/Dig-MNIST labels.csv")
df_3 = pd.read_csv("dataset/test labels.csv")

# Create the datasets
ds_1 = ds.KannadaMNIST( image_folder="dataset/train"
                      , labels      =df_1
                      , transforms  =transform
                      )

ds_2 = ds.KannadaMNIST( image_folder="dataset/Dig-MNIST"
                      , labels      =df_2
                      , transforms  =transform
                      )

# Create loaders
loader_1 = torch.utils.data.DataLoader(dataset=ds_1, batch_size=128, shuffle=False)
loader_2 = torch.utils.data.DataLoader(dataset=ds_2, batch_size=128, shuffle=False)

#%% Ensemble accuracy
with open("dataset\\k-fold_predictions.pth", 'rb') as f:
    predictions = load(f)

# Training set
predictions_ = torch.argmax(torch.stack([result["predictions"] for result in predictions["train"]], dim=1).mean(dim=1), dim=1)
labels       = predictions["train"][0]["labels"]

ensemble_train_accuracy = torch.eq(labels, predictions_).float().mean()

# Dig-MNIST set
predictions_ = torch.argmax(torch.stack([result["predictions"] for result in predictions["Dig-MNIST"]], dim=1).mean(dim=1), dim=1)
labels       = predictions["Dig-MNIST"][0]["labels"]

ensemble_dig_mnist_accuracy = torch.eq(labels, predictions_).float().mean()

#%% Extended accuracy
if False:
    # Prediction loss
    predict_loss = Loss.CrossEntropyConfidenceLoss(reduce=False)

    # Instantiate the model
    model = CustomNetV2.KannadaNet(input_shape=[1, 28, 28], n_class=10)

    # Load the weights
    model.load_state_dict(torch.load("checkpoints/CustomNetV2-Extended Best Weights.tar")["model_state_dict"])

    # Activate evaluation
    model.eval()
    model.cuda()
    extended_predictions = {}

    for name, loader in zip(["train", "Dig-MNIST"], [loader_1, loader_2]):
        with torch.no_grad():
            extended_predictions[name] = [Utils.predict( loader =loader
                                                       , net    =model
                                                       , loss_fn=predict_loss if name is not "test" else None
                                                       )
                                         ]

    # Save results
    with open("dataset\\extended_predictions.pth", 'wb') as f:
        dump(extended_predictions, f)

# Load the precomputed results
with open("dataset\\extended_predictions.pth", 'rb') as f:
    extended_predictions = load(f)

# Training set
predictions_ = torch.argmax(extended_predictions["train"][0]["predictions"], dim=1)
labels       = extended_predictions["train"][0]["labels"]

extended_train_accuracy = torch.eq(labels, predictions_).float().mean()

# Dig-MNIST set
predictions_ = torch.argmax(extended_predictions["Dig-MNIST"][0]["predictions"], dim=1)
labels       = extended_predictions["Dig-MNIST"][0]["labels"]

extended_dig_mnist_accuracy = torch.eq(labels, predictions_).float().mean()

#%% Simple mean between ensemble and extended models
# Training set
predictions_ = torch.argmax(torch.stack( [ torch.stack([result["predictions"]
                                                       for result in predictions["train"]
                                                       ], dim=1
                                                      ).mean(dim=1)
                                         , extended_predictions["train"][0]["predictions"]
                                         ]
                                       , dim=1
                                       ).mean(dim=1)
                           , dim=1
                           )

labels = extended_predictions["train"][0]["labels"]

mean_train_accuracy = torch.eq(labels, predictions_).float().mean()

# Dig-MNIST
predictions_ = torch.argmax(torch.stack( [ torch.stack([result["predictions"]
                                                       for result in predictions["Dig-MNIST"]
                                                       ], dim=1
                                                      ).mean(dim=1)
                                         , extended_predictions["Dig-MNIST"][0]["predictions"]
                                         ]
                                       , dim=1
                                       ).mean(dim=1)
                           , dim=1
                           )

labels = extended_predictions["Dig-MNIST"][0]["labels"]

mean_dig_mnist_accuracy = torch.eq(labels, predictions_).float().mean()

a=0

#%% Build a secondary dataset
train_dataset_secondary = torch.stack([result["predictions"]
                                       for result in predictions["train"]
                                      ] + [extended_predictions["train"][0]["predictions"]]
                                     , dim=1
                                     )
train_labels_secondary = extended_predictions["train"][0]["labels"]

dig_mnist_dataset_secondary = torch.stack([result["predictions"]
                                           for result in predictions["Dig-MNIST"]
                                          ] + [extended_predictions["Dig-MNIST"][0]["predictions"]]
                                         , dim=1
                                         )

dig_mnist_labels_secondary = extended_predictions["Dig-MNIST"][0]["labels"]

# Save results
with open("dataset\\seconday_dataset.pth", 'wb') as f:
    dump({ "train"    : {"X": train_dataset_secondary.numpy(),     "y": train_labels_secondary.numpy()}
         , "Dig-MNIST": {"X": dig_mnist_dataset_secondary.numpy(), "y": dig_mnist_labels_secondary.numpy()}
         }, f)
