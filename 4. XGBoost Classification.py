"""
Uses model outputs as input to an XGBoost classifier
"""
#%% Setup
import numpy  as np
import scipy.stats as st

from sklearn.model_selection import train_test_split, cross_val_score, RandomizedSearchCV
from xgboost.sklearn import XGBClassifier

from joblib import dump, load

#%% Data preparation
# Load the datasets
with open("dataset\\seconday_dataset.pth", 'rb') as f:
    seconday_dataset = load(f)

# Combine the features and labels
X = np.concatenate([seconday_dataset["train"]["X"], seconday_dataset["Dig-MNIST"]["X"]])
y = np.concatenate([seconday_dataset["train"]["y"], seconday_dataset["Dig-MNIST"]["y"]])

# Training test split
X_train, X_test, y_train, y_test = train_test_split(X.reshape((X.shape[0], -1)), y, test_size=0.01, random_state=42)

#%% Training
if True:
    one_to_left = st.beta(10, 1)
    from_zero_positive = st.expon(0, 50)

    params = { "n_estimators"    : st.randint(3, 40)
             , "max_depth"       : st.randint(3, 40)
             , "learning_rate"   : st.uniform(0.05, 0.4)
             , "colsample_bytree": one_to_left
             , "subsample"       : one_to_left
             , "gamma"           : st.uniform(0, 10)
             , 'reg_alpha'       : from_zero_positive
             , "min_child_weight": from_zero_positive
             }

    xclas = XGBClassifier(nthreads=-1)  # and for classifier
    gs = RandomizedSearchCV(xclas, params, n_jobs=1)
    gs.fit(X_train, y_train)

    # Save the trained model
    dump(gs.best_estimator_, "checkpoints/kannda_xgboost.joblib.dat")

#%% Prediction
# Check against the additional dataset
X = seconday_dataset["Dig-MNIST"]["X"]
y = seconday_dataset["Dig-MNIST"]["y"]

# Load the model
xclas = load("checkpoints/kannda_xgboost.joblib.dat")

# Check the class distribution
y_ = xclas.predict(X.reshape((X.shape[0], -1)))

prediction_counts = np.unique(y_, return_counts=True)
