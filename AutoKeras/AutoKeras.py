'''
A notebook for exploring the capabilities of AutoKeras using the KannadaMNIST dataset from Kaggle
'''
#%% Setup
import numpy      as np
import pandas     as pd
import autokeras  as ak
import tensorflow as tf

from tensorflow.keras.preprocessing.image import ImageDataGenerator

from Dataset import image_dataset, show_batch, prepare_for_training

import matplotlib.pyplot as plt

#%% Dataset
# Load the labels
df_train = pd.read_csv("dataset\\train labels.csv")
df_test = pd.read_csv("dataset\\Dig-MNIST labels.csv")

# Update the id
df_train["id"] = df_train["id"] + ".png"
df_test["id"] = df_test["id"] + ".png"

# Create the dataset
train_ds = image_dataset( data_dir    ="dataset\\train\\"
                        , label_df    =df_train
                        , image_width =28
                        , image_height=28
                        , channels    =3
                        )

test_ds = image_dataset( data_dir    ="dataset\\Dig-MNIST\\"
                        , label_df    =df_test
                        , image_width =28
                        , image_height=28
                        , channels    =3
                        )

# Check the output shapes
for image, label in train_ds.take(1):
    print("Image shape: ", image.numpy().shape)
    print("Label: ", label.numpy())

# Prepare the dataset for training
train_ds = prepare_for_training(train_ds, batch_size=128)
test_ds = prepare_for_training(test_ds, batch_size=128)

# View a sample
image_batch, label_batch = next(iter(train_ds))
show_batch(image_batch.numpy(), label_batch.numpy())
'''

#%% Dataset - old
# Training
# Load the labels
df_train = pd.read_csv("dataset\\train labels.csv")

# Update the id
df_train["id"] = df_train["id"] + ".png"

# Update the column type of the label
df_train["label"] = df_train["label"].astype(str)

# Testing
# Load the dataset
df_test = pd.read_csv("dataset\\Dig-MNIST labels.csv")

# Update the id
df_test["id"] = df_test["id"] + ".png"

# Update the column type of the label
df_test["label"] = df_test["label"].astype(str)

#%% Data generator
# Train dataset
train_datagen = ImageDataGenerator()
train_generator = train_datagen.flow_from_dataframe( dataframe  =df_train
                                                   , directory  = "dataset\\train\\"
                                                   , x_col      ="id"
                                                   , y_col      ="label"
                                                   , class_mode ="sparse"
                                                   , shuffle    =True
                                                   , target_size=[28,28]
                                                   , color_mode ="grayscale"
                                                   , batch_size =100
                                                   )

tf_train_dataset = tf.data.Dataset.from_generator( generator    =lambda: train_generator
                                                 , output_types =(tf.float32, tf.int32)
                                                 , output_shapes=(tf.TensorShape([100,28,28,1]), tf.TensorShape(100))
                                                 )

# Test dataset
test_datagen = ImageDataGenerator()
test_generator = test_datagen.flow_from_dataframe( dataframe  =df_test
                                                 , directory  = "dataset\\Dig-MNIST\\"
                                                 , x_col      ="id"
                                                 , y_col      ="label"
                                                 , class_mode ="sparse"
                                                 , shuffle    =True
                                                 , target_size=[28,28]
                                                 , color_mode ="grayscale"
                                                 , batch_size =1024
                                                 )

tf_test_dataset = tf.data.Dataset.from_generator( generator    =lambda: test_generator
                                                , output_types =(tf.float32, tf.int32)
                                                , output_shapes=(tf.TensorShape([1024,28,28,1]), tf.TensorShape(1024))
                                                )

# Store the entire dataset
# Training
x_train = []
y_train = []
for x_, y_ in train_generator:
    x_train.append(x_)
    y_train.append(y_)
    if len(x_train) > 60000 / 128:
        break

x_train = np.concatenate(x_train)
y_train = np.concatenate(y_train)
'''
# Testing
x_test = []
y_test = []
for x_, y_ in test_ds:
    x_test.append(x_)
    y_test.append(y_)
    if len(x_test) > 10240 / 128:
        break

x_test = np.concatenate(x_test)
y_test = np.concatenate(y_test)

#%% AutoKeras model
clf = ak.ImageClassifier(max_trials=100)
clf.fit(train_ds, validation_data=(x_test, y_test), epochs=1000, batch_size=128)
'''
#%% AutoKeras model
input_node = ak.ImageInput()
x = ak.Normalization()(input_node)
x = ak.ImageAugmentation(percentage=1.0)(x)
x_1 = ak.ConvBlock()(x)
x_2 = ak.ResNetBlock(version='v2')(x)
x = ak.Merge()([x_1, x_2])
x = ak.ClassificationHead()(x)

auto_model = ak.AutoModel(inputs=input_node, outputs=x, max_trials=20)
auto_model.fit(x=x_train, y=y_train, validation_data=(x_test, y_test), epochs=1000, batch_size=128)

'''
