"""
A implementation of a TensorFlow data.Dataset API dataset
"""
#%% Setup
import os
import tensorflow        as tf
import matplotlib.pyplot as plt

#%% Dataset function
def image_dataset(data_dir, label_df, image_width, image_height, channels=1):
    # Create a list of images
    list_ds = tf.data.Dataset.list_files(data_dir + "*.png")

    # Constants
    IMG_WIDTH    = image_width
    IMG_HEIGHT   = image_height
    IMG_CHANNELS = channels
    LABEL_DF     = label_df

    def get_label(file_path):
        # Determine the label of an image
        image = tf.strings.split(file_path, os.path.sep)[-1]

        # Query the label DataFrame
        #return int(LABEL_DF.query("id == {}".format(str(image)))["label"].values[0])
        return 0

    def decode_img(img):
        # Convert the compressed string to a 3D uint8 tensor
        img = tf.image.decode_png(img, channels=IMG_CHANNELS)

        # Use `convert_image_dtype` to convert to floats in the [0,1] range.
        img = tf.image.convert_image_dtype(img, tf.float32)

        # Resize the image to the desired size.
        return tf.image.resize(img, [IMG_WIDTH, IMG_HEIGHT])

    def process_path(file_path):
        # Determine the label of the images
        label = get_label(file_path)

        # Load the raw data from the file as a string
        img = tf.io.read_file(file_path)

        # Convert to a tensor representation
        img = decode_img(img)

        return img, label

    # Create the dataset
    return list_ds.map(process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE)

#%% Utility functions
def show_batch(image_batch, label_batch):
    # Initialize a plot
    plt.figure(figsize=(10,10))

    # Plot a number of samples
    for n in range(25):
        ax = plt.subplot(5,5,n+1)
        plt.imshow(image_batch[n])
        plt.title(label_batch[n])
        plt.axis('off')

def prepare_for_training(ds, batch_size, cache=True, shuffle_buffer_size=1000):
    # This is a small dataset, only load it once, and keep it in memory.
    # use `.cache(filename)` to cache preprocessing work for datasets that don't
    # fit in memory.
    if cache:
        if isinstance(cache, str):
          ds = ds.cache(cache)
        else:
          ds = ds.cache()

    # Shuffle
    ds = ds.shuffle(buffer_size=shuffle_buffer_size)

    # Repeat forever
    ds = ds.repeat()
    ds = ds.batch(batch_size)

    # `prefetch` lets the dataset fetch batches in the background while the model
    # is training.
    ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

    return ds
