"""
Pure Keras implementation
"""
#%% Setup
import numpy      as np
import pandas     as pd
import tensorflow as tf

from tensorflow.keras.preprocessing.image import ImageDataGenerator

from model import CustomNet

#%% Dataset
dfs = {}
for name, path in zip(["train", "test"], ["train labels.csv", "Dig-MNIST labels.csv"]):
    # Load the labels
    df = pd.read_csv("dataset\\{}".format(path))

    # Update the id
    df["id"] = df["id"] + ".png"

    # Update the column type of the label
    df["label"] = df["label"].astype(str)

    dfs[name] = df


#%% Data generator
# Train dataset
train_datagen = ImageDataGenerator( rotation_range    =15
                                  , width_shift_range =0.15
                                  , height_shift_range=0.15
                                  , shear_range       =10
                                  , zoom_range        =0.1
                                  , rescale           =1./255
                                  )
train_generator = train_datagen.flow_from_dataframe( dataframe  =dfs["train"]
                                                   , directory  = "dataset\\train\\"
                                                   , x_col      ="id"
                                                   , y_col      ="label"
                                                   , class_mode ="sparse"
                                                   , shuffle    =True
                                                   , target_size=[28,28]
                                                   , color_mode ="grayscale"
                                                   , batch_size =100
                                                   )

# Test dataset
test_datagen = ImageDataGenerator(rescale=1./255)
test_generator = test_datagen.flow_from_dataframe( dataframe  =dfs["test"]
                                                 , directory  = "dataset\\Dig-MNIST\\"
                                                 , x_col      ="id"
                                                 , y_col      ="label"
                                                 , class_mode ="sparse"
                                                 , shuffle    =True
                                                 , target_size=[28,28]
                                                 , color_mode ="grayscale"
                                                 , batch_size =1024
                                                 )

#%% Callbacks
# Scheduler
def schedule(epoch):
    if epoch < 10:
        return float((epoch + 1) * 0.001)
    if epoch > 10:
        return float(0.01 / ((epoch - 10) ** 1.2))

#%% Training
# Loop through different model configurations
for test_size in [0.2, 0.1, 0.05, 0.05]:
    for fc_pool in ["max", "global"]:
        # Callbacks
        # Learning rate adjustment
        lr_schedule = tf.keras.callbacks.LearningRateScheduler(schedule)
        reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor="val_loss", factor=0.2, patience=4)

        # Early stopping
        es = tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=10)

        # Checkpoint
        checkpoint = tf.keras.callbacks.ModelCheckpoint( filepath      ="checkpoints\\{}-pool-{}-test-size.hdf5".format(fc_pool, test_size)
                                                       , monitor       ="val_loss"
                                                       , save_best_only=True
                                                       , save_freq     ="epoch"
                                                       )

        # Create a model
        model = CustomNet( input_shape=[28,28,1]
                         , n_class    =10
                         , fc_pooling =fc_pool
                         )

        # Compile the model
        model.compile( optimizer="rmsprop"
                     , loss     =tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
                     , metrics  =[tf.keras.metrics.SparseCategoricalAccuracy()]
                     )

        # Train the model
        model.fit( train_generator
                 , validation_data=test_generator
                 , epochs         =100
                 , callbacks      =[reduce_lr, es, checkpoint]
                 )
