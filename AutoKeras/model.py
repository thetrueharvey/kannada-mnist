"""
A Keras model
"""
#%% Setup
from tensorflow import float32 as tf_float32

from tensorflow.keras.layers  import Input, Conv2D, MaxPooling2D, GlobalAveragePooling2D, BatchNormalization, PReLU, Dropout, Flatten, Dense, Softmax
from tensorflow.keras         import Model

#%% Network function
def CustomNet(input_shape, n_class, fc_pooling="max"):
    # Create the input
    model_input = Input(shape=input_shape)

    # First section
    x = _conv_block(model_input, filters=64, n=3)
    x = MaxPooling2D(pool_size=2, strides=2)(x)

    # Second section
    x = _conv_block(x, filters=128, n=3)
    x = MaxPooling2D(pool_size=2, strides=2)(x)

    # Third section
    x = _conv_block(x, filters=256, n=2)

    if fc_pooling == "max":
        x = MaxPooling2D(pool_size=2, strides=2)(x)
    else:
        x = GlobalAveragePooling2D()(x)

    # Fully connected layers
    x = Flatten()(x)
    x = _fc_layers(x, [256, 256])

    logits = Dense(units=n_class)(x)

    # Create a model
    model = Model( inputs =model_input
                 , outputs=logits
                 )

    model.summary()

    return model

def _conv_bn_activation(x, filters):
    x = Conv2D(filters=filters, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)

    return PReLU(alpha_initializer="uniform")(x)

def _conv_block(x, filters, n):
    for _ in range(n):
        x = _conv_bn_activation(x, filters)

    return x

def _fc_layers(x, units):
    for n in units:
        x = Dropout(rate=0.2)(x)
        x = Dense(units=n)(x)
        x = PReLU(alpha_initializer="uniform")(x)
        x = BatchNormalization()(x)

    return x
