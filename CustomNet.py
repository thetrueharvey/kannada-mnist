"""
Network for Kannada MNIST classification
"""
#%% Setup
import torch
import torch.nn            as nn
import torch.nn.functional as F

from torchtools.nn import SimpleSelfAttention

#%% Network
class KannadaNet(nn.Module):
    def __init__( self
                , input_shape
                , n_class
                , triplet    =True
                ):
        """
        Builds a model for the Kannada MNIST dataset
        Based on: https://www.kaggle.com/bustam/cnn-in-keras-for-kannada-digits
        :param input_shape:
        :param n_class:
        """
        super(KannadaNet, self).__init__()
        self.triplet = triplet
        
        # Build the layers
        self._build_layers(input_shape=input_shape, n_class=n_class)

        # Initialize weights
        self._init_weight()

        # Parameter counts
        print("Model contains {} parameters".format(sum(p.numel() for p in self.parameters() if p.requires_grad)))
    
    def forward(self, x):
        if self.triplet:
            return self._triplet_forward(x)
        else:
            return self._forward(x)
    
    def _forward(self, x):
        x = self._conv_forward(x)

        # Classification
        x = self.fc_1(x)
        x = F.leaky_relu(x)
        x = self.bn_1(x)

        x = self.fc_2(x)
        x = F.leaky_relu(x)
        x = self.bn_2(x)

        #confidence = self.confidence(x)
        out = self.classifier(x)

        return {"predictions": out.cpu().float()} #, "confidence": confidence.cpu().float()}

    def _triplet_forward(self, x):
        anchor_embeddings   = self._forward(x["anchor"].cuda())["predictions"]
        positive_embeddings = self._forward(x["positive"].cuda())["predictions"]
        negative_embeddings = self._forward(x["negative"].cuda())["predictions"]

        return {"anchor": anchor_embeddings, "positive":positive_embeddings, "negative": negative_embeddings}

    def _build_layers(self, input_shape, n_class):
        """
        Adds layers to the network
        :param input_shape:
        :param n_class:
        :return:
        """
        self.conv_1 = nn.ModuleList([ Conv2D_BN( in_channels =input_shape[0]
                                               , out_channels=64
                                               , padding     =1
                                               )
                                    , Conv2D_BN( in_channels =64
                                               , out_channels=64
                                               , padding     =1
                                               )
                                    , Conv2D_BN( in_channels =64
                                               , out_channels=64
                                               , padding     =1
                                               )
                                    , SimpleSelfAttention(64)
                                    , nn.MaxPool2d(kernel_size=2, stride=2)
                                    #, nn.Dropout(p=0.2)
                                    ]
                                   )

        self.conv_2 = nn.ModuleList([ Conv2D_BN( in_channels =64
                                               , out_channels=128
                                               , padding     =1
                                               )
                                    , Conv2D_BN( in_channels =128
                                               , out_channels=128
                                               , padding     =1
                                               )
                                    , Conv2D_BN( in_channels =128
                                               , out_channels=128
                                               , padding=1
                                               )
                                    , SimpleSelfAttention(128)
                                    , nn.MaxPool2d(kernel_size=2, stride=2)
                                    #, nn.Dropout(p=0.2)
                                    ]
                                   )

        self.conv_3 = nn.ModuleList([ Conv2D_BN( in_channels =128
                                               , out_channels=256
                                               , padding     =1
                                               )
                                    , Conv2D_BN( in_channels =256
                                               , out_channels=256
                                               , padding     =1
                                               )
                                    , SimpleSelfAttention(256)
                                    , nn.MaxPool2d(kernel_size=2, stride=2)
                                    #, nn.Dropout(p=0.2)
                                    ]
                                   )

        # Determine the output dimensions
        conv_out_dims = self._conv_forward(torch.zeros([1] + input_shape, dtype=torch.float32))

        # Fully connected layers
        self.fc_1 = nn.Linear(in_features=conv_out_dims.shape[-1], out_features=256)

        self.bn_1 = nn.BatchNorm1d(num_features=256)
        self.fc_2 = nn.Linear(in_features=256, out_features=256)

        self.bn_2 = nn.BatchNorm1d(num_features=256)

        #self.confidence = nn.Linear(in_features=256, out_features=1)
        self.classifier = nn.Linear(in_features=256, out_features=n_class)

    # Convolutional forward
    def _conv_forward(self, x):
        """
        Forward function for convolutional layers
        :param x:
        :return:
        """
        x = block_forward(self.conv_1, x)
        x = block_forward(self.conv_2, x)
        x = block_forward(self.conv_3, x)

        # Flatten
        x = torch.flatten(x, start_dim=1)

        return x

    # Weight initialization
    def _init_weight(self):
        for m in self.modules():
            print(m)
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1.0)
                m.bias.data.zero_()



#%% Utility functions
class Conv2D_BN(nn.Module):
    def __init__(self, in_channels, out_channels, kernel=3, stride=1, padding=1, activation=True):
        """
        Convolution followed by Batch Normalization, with optional Mish activation
        :param in_features:
        :param out_features:
        :param kernel_size:
        :param stride:
        :param padding:
        :param groups:
        :param activation:
        """
        super(Conv2D_BN, self).__init__()

        # Class parameters
        self.activation = activation

        # Convolution and BatchNorm class
        self.conv = nn.Conv2d( in_channels =in_channels
                             , out_channels=out_channels
                             , kernel_size =kernel
                             , stride      =stride
                             , padding     =padding
                             , bias        =True
                             )

        self.bn = nn.BatchNorm2d(num_features=out_channels, momentum=0.9, eps=1e-5)

    def forward(self, x):
        x = self.conv(x)
        if self.activation:
            x = F.leaky_relu(x)
        x = self.bn(x)

        return x

def block_forward(layers, x):
    for layer in layers:
        x = layer(x)

    return x

# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))
