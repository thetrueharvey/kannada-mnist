"""
Custom dataset class
"""

#%% Setup
import os
import torch
import torchvision.transforms.functional as FT

import numpy  as np
import pandas as pd

from PIL       import Image

#%% Dataset class
class KannadaMNIST(object):
    def __init__( self
                , image_folder    : str
                , labels          : pd.DataFrame
                , transforms      : list  = None
                ):
        """
        Class for creating a dataset for training the flow chart object detector
        """
        # Class attributes
        self.image_folder = image_folder
        self.labels       = labels
        self.transforms   = transforms

        # Create an index list
        self.index = np.arange(self.labels.shape[0])

    def __len__(self):
        return len(self.index)

    def __getitem__(self, idx):
        # Load the labels
        label = self.labels.iloc[self.index[idx]]

        # Image ID
        id_ = label["id"]

        # Load the image
        img = Image.open(self.image_folder + "/" + label["id"] + ".png")

        # Populate the labels
        label = label["label"]

        # Apply transformations
        img_ = self.transforms(img)

        return {"image": img_, "label": label, "id": id_}

    def drop_extrema(self, ids, losses, p_easy=0.05, p_hard=0.002):
        """
        Removes the best p% samples from the dataset, pushing the network to learn harder examples
        :param p:
        :return:
        """
        # Create a table
        df = pd.DataFrame({ "id"  :ids
                          , "loss":losses
                          }
                         ).sort_values(by="loss")

        # Determine the labels to keep
        keep_labels = df.iloc[int(len(df) * p_easy):(len(df) - int(len(df) * p_hard))]["id"].values

        # Update the labels and index
        self.labels = self.labels[self.labels["id"].isin(keep_labels)]
        self.index  = np.arange(self.labels.shape[0])
