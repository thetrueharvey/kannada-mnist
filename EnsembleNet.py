"""
Creates an ensemble of networks
"""
#%% Setup

#%% Class
class EnsembleNetwork:
    def __init__(self, base_net, input_shape, n_classes:list):
        # Create the networks
        self.nets = [base_net(input_shape, n_class) for n_class in n_classes]

    def __call__(self, x):
        return [net(x_) for net, x_ in zip(self.nets, x)]
