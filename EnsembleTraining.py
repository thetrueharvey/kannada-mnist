"""
Script for training an ensemble network
"""
# Results
# RMSProp with OneStepCycle into Schedule, with transforms

#%% Setup

import numpy  as np
import pandas as pd

import torch
import torch.nn            as nn
import torch.nn.functional as F

import classification_transforms as T
import Dataset                   as ds
import MobileNetV2
import CustomNet
import CustomNetV2
import Loss
import Utils
import Logger

from torchvision      import transforms
from torchtools.optim import RangerLars

torch.cuda.is_available()

#%% Dataset
# Train transform
train_transform = transforms.Compose([ T.ChooseOne([ #T.RandomNoise(decay=1/600e3, decay_stop=0.8, probability=0.5)
                                                     T.RandomErasures(n_erasures=[1,3], h=[0.15, 0.4], w=[0.15, 0.4],  decay=1/600e3, decay_stop=1.0, probability=1.0)
                                                   , T.RandomAffine(decay=1/600e3, decay_stop=1.0, probabilities=[0.7,0.7,0.7,0.7,0.7,0.7])
                                                     #, T.RandomCrop(decay=1/600e3, decay_stop=0.3, probability=0.5)
                                                     #, T.RandomColourJitter(decay=1/600e3, decay_stop=0.1, probabilities=[0.2,0.2,0.2,0.2,0.2])
                                                     #, T.Skew(probability=0.1)
                                                   , T.RandomDistortion(probability=1.0, grid=[5,5], magnitude=9)
                                                   ]
                                                  )
                                     , transforms.ToTensor()
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([transforms.ToTensor()])

# Create the datasets
train_df = pd.read_csv("dataset/train labels.csv")
test_df  = pd.read_csv("dataset/test labels.csv")

if False:
    for i in range(10):
        train_dataset = ds.KannadaMNIST( image_folder="dataset/train images"
                                       , labels      =train_df
                                       , transforms  =train_transform
                                       , classes     =[i]
                                       )

        test_dataset = ds.KannadaMNIST( image_folder="dataset/test images"
                                      , labels      =test_df
                                      , transforms  =test_transform
                                      , classes=[i]
                                      )

        # Create a loader
        train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=512, shuffle=True)
        test_loader  = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=1024, shuffle=False)

        # Network components
        net = CustomNet.KannadaNet( input_shape=[1, 28, 28]
                                  , n_class    =1
                                  , triplet    =False
                                  )

        net.cuda()

        loss_fn = Loss.SingleClassLoss()

        optimizer = torch.optim.RMSprop(params=net.parameters())
        scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr=0.05, steps_per_epoch=len(train_loader), epochs=60)

        # Training
        best_acc, epoch = Utils.train( name          ="CustomNet Binary Class {}".format(i)
                                     , train_loader  =train_loader
                                     , test_loader   =test_loader
                                     , net           =net
                                     , loss_fn       =loss_fn
                                     , optimizer     =optimizer
                                     , scheduler     =scheduler
                                     , scheduler_type="per batch"
                                     , logger        =Logger.SingleClassLogger
                                     , epoch_end     =60
                                     , early_stopping=60
                                     )

        optimizer = torch.optim.RMSprop(params=net.parameters())
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau( optimizer=optimizer
                                                              , mode     ="min"
                                                              , factor   =0.5
                                                              , patience =2
                                                              , verbose  =1
                                                              , min_lr   =1e-9
                                                              , cooldown =0
                                                              )

        best_acc, epoch = Utils.train( name          ="CustomNet Binary Class {}".format(i)
                                     , train_loader  =train_loader
                                     , test_loader   =test_loader
                                     , net           =net
                                     , loss_fn       =loss_fn
                                     , optimizer     =optimizer
                                     , scheduler     =scheduler
                                     , scheduler_type="per epoch"
                                     , logger        =Logger.SingleClassLogger
                                     , epoch_end     =60
                                     , early_stopping=60
                                     )

#%% Prediction
# Dataset
dataset = ds.KannadaMNIST( image_folder="dataset/test images"
                         , labels      =test_df
                         , transforms  =test_transform
                         )
loader  = torch.utils.data.DataLoader(dataset=dataset, batch_size=5000, shuffle=False)

# Targets
targets = torch.cat([x["label"] for x in loader])

# Predictions
predictions = []
for i in range(10):
    net = CustomNet.KannadaNet( input_shape=[1, 28, 28]
                              , n_class    =1
                              , triplet    =False
                              )

    net.load_state_dict(torch.load("checkpoints/CustomNet Binary Class {} Best Weights.tar".format(i))["model_state_dict"], strict=False)
    net.cuda()
    net.eval()

    with torch.no_grad():
        prediction = [net(x["image"].cuda())["predictions"] for x in loader]

    predictions.append(torch.cat(prediction))

# Predictions
predictions = torch.cat(predictions, dim=1)
predictions = predictions.argmax(dim=1)

accuracy = torch.eq(targets, predictions).float().mean()
a=0


