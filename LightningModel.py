"""
Creates a PyTorch Lightning model for easy training
"""
#%% Setup
import torch
import torch.nn.functional as F
import pytorch_lightning   as pl

#%% Model class
class KannadaMNISTModel(pl.LightningModule):
    def __init__( self
                , train_loader
                , val_loader
                , net
                , loss
                , optimizer
                ):
        super(KannadaMNISTModel, self).__init__()

        # Class attributes
        self.train_loader = train_loader
        self.val_loader   = val_loader
        self.net          = net
        self.loss_fn      = loss
        self.optimizer    = optimizer

        # Instantiate validation loss for the learning rate scheduler
        self.val_loss = 0

    def forward(self, x):
        return self.net(x)

    def training_step(self, batch, batch_idx):
        y_   = self.forward(batch["image"])
        loss = self.loss_fn(y_, batch["label"])

        # Calculate the prediction accuracy
        accuracy = torch.eq( torch.argmax(F.softmax(y_["prediction"], dim=-1), dim=1)
                           , batch["label"]
                           ).float().mean()

        # Calculate the confidence for correct and incorrect samples
        #correct_confidence   = y_["confidence"][y_["prediction"] == batch["label"]].mean().detach().numpy()
        #incorrect_confidence = y_["confidence"][y_["prediction"] != batch["label"]].mean().detach().numpy()

        return {"loss": loss, "train_acc": accuracy, "log": {"train_loss": loss, "train_acc": accuracy}}

    def validation_step(self, batch, batch_idx, dataloader_idx):
        y_ = self.forward(batch["image"])
        loss = self.loss_fn(y_, batch["label"])

        # Calculate the prediction accuracy
        accuracy = torch.eq( torch.argmax(F.softmax(y_["prediction"], dim=-1), dim=1)
                           , batch["label"]
                           ).float().mean()

        return {"val_loss": loss, "val_acc": accuracy, "log": {"val_loss": loss, "val_acc": accuracy}}

    def validation_end(self, outputs):
        # Validation set loss
        avg_loss = torch.stack([x['val_loss'] for x in outputs[0]]).mean()
        self.val_loss = avg_loss

        # Dig-MNIST accuracy
        dig_mnist_accuracy = torch.stack([x['val_acc'] for x in outputs[1]]).mean()

        tensorboard_logs = {'val_loss': avg_loss, "dig_mnist_acc": dig_mnist_accuracy}
        return {'avg_val_loss': avg_loss, 'log': tensorboard_logs}

    def configure_optimizers(self):
        return self.optimizer

    def train_dataloader(self):
        return self.train_loader

    def val_dataloader(self):
        return self.val_loader
