"""
Custom accuracy measure
"""
#%% Setup
import pandas as pd
import numpy  as np

import torch
import torch.nn.functional as F


#%% Accuracy class
class Logger:
    def __init__(self):
        self.results = pd.DataFrame()

        self.epochs               = []
        self.loss                 = []
        self.accuracy             = []
        self.correct_confidence   = []
        self.incorrect_confidence = []

    def __call__(self, out, labels, epoch, loss):
        # Calculate the predictions
        predictions = torch.argmax(F.softmax(out["prediction"]), dim=1)

        # Calculate the prediction accuracy
        self.accuracy.append(torch.eq(predictions, labels).float().mean().numpy())

        # Calculate the confidence for correct and incorrect samples
        self.correct_confidence.append(out["confidence"][predictions == labels].mean().detach().numpy())
        self.incorrect_confidence.append(out["confidence"][predictions != labels].mean().detach().numpy())

        # Loss
        self.loss.append(loss)

    def print_latest(self, n=10):
        # Accuracy
        accuracy = np.mean(self.accuracy[-n:])

        # Confidence
        correct_confidence   = np.mean(self.correct_confidence[-n:])
        incorrect_confidence = np.mean(self.incorrect_confidence[-n:])

        # Loss
        loss = np.mean(self.loss[-n:])

        return "Loss: {0:.3f} Prediction Accuracy: {1:.3f} Correct Confidence: {2:.3f} Incorrect Confidence: {3:.3f}".format(loss, accuracy, correct_confidence, incorrect_confidence)

    #def print_epoch(self):

class SingleClassLogger:
    def __init__(self):
        self.results = pd.DataFrame()

        self.epochs       = []
        self.loss         = []
        self.accuracy     = []

    def __call__(self, out, labels, epoch, loss):
        # Calculate the predictions
        predictions = (F.sigmoid(out["predictions"]) > 0.5).float().squeeze()

        # Calculate the prediction accuracy
        self.accuracy.append(torch.eq(predictions, labels).float().mean().numpy())

        # Loss
        self.loss.append(loss)

    def print_latest(self, n=10):
        # Accuracy
        accuracy = np.mean(self.accuracy[-n:])

        # Loss
        loss = np.mean(self.loss[-n:])

        return "Loss: {0:.3f} Prediction Accuracy: {1:.3f}".format(loss, accuracy)

    #def print_epoch(self):

class SiameseLogger:
    def __init__(self):
        self.results = pd.DataFrame()

        self.epochs               = []
        self.loss                 = []
        self.accuracy             = []


    def __call__(self, out, epoch, loss):
        # Calculate the predictions
        # Distance between positive pairs
        distance_positive = (out["anchor"].cpu() - out["positive"].cpu()).pow(2).sum(1)

        # Distance between negative pairs
        distance_negative = (out["anchor"].cpu() - out["negative"].cpu()).pow(2).sum(1)

        # Accuracy simply checks if positive distance is smaller than negative distance
        self.accuracy.append((distance_positive < distance_negative).numpy().mean())

        # Loss
        self.loss.append(loss)

    def print_latest(self, n=10):
        # Accuracy
        accuracy = np.mean(self.accuracy[-n:])

        # Confidence
        #correct_confidence   = np.mean(self.correct_confidence[-n:])
        #incorrect_confidence = np.mean(self.incorrect_confidence[-n:])

        # Loss
        loss = np.mean(self.loss[-n:])

        return "Loss: {0:.3f} Prediction Accuracy: {1:.3f}".format(loss, accuracy)

    #def print_epoch(self):