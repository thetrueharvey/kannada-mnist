"""
Custom loss functions
"""
#%% Setup
import torch
import torch.nn            as nn
import torch.nn.functional as F

#%% Hard negative loss
class CrossEntropyHardNegative(nn.Module):
    def __init__(self, ratio):
        """
        CrossEntropy loss with hard-negative mining
        :param ratio: float. The ratio of results that should be considered to be 'hard negative'
        """
        super(CrossEntropyHardNegative, self).__init__()

        # Class attributes
        self.ratio = ratio

        # Instantiate the loss
        self.loss = nn.CrossEntropyLoss(reduction="none")

    def forward(self, out, labels):
        # Calculate the per-element loss
        loss = self.loss(out.cpu(), labels)

        # Order the loss
        loss, _ = loss.sort(descending=True)

        # Determine the hard and easy loss
        hard_loss = loss[:(int(loss.shape[0] * self.ratio))]
        easy_loss = loss[(int(loss.shape[0] * self.ratio)):]

        # Reduce
        loss = (hard_loss.sum() + easy_loss.sum()) / easy_loss.shape[0]

        return loss


class CrossEntropyConfidenceLoss(nn.Module):
    def __init__(self, reduce=True):
        super(CrossEntropyConfidenceLoss, self).__init__()

        # Class parameters
        self.reduce = reduce

        self.prediction_loss = nn.CrossEntropyLoss(reduction="none")
        self.confidence_loss = nn.MSELoss(reduction="none")

    def forward(self, out, labels):
        # Calculate the prediction loss
        prediction_loss = self.prediction_loss(out["prediction"], labels)

        # Hence calculate the confidence loss
        confidence_loss = self.confidence_loss(out["confidence"].squeeze(), prediction_loss)

        if self.reduce:
            return prediction_loss.mean() + confidence_loss.mean()
        else:
            return prediction_loss + confidence_loss

class SingleClassLoss(nn.Module):
    def __init__(self):
        super(SingleClassLoss, self).__init__()

        self.loss = nn.BCEWithLogitsLoss()

    def forward(self, out, labels):
        # Calculate the loss
        loss = self.loss(out["predictions"].squeeze(), labels.float())

        return loss

class TripletLoss(nn.Module):
    def __init__(self, margin):
        super(TripletLoss, self).__init__()

        self.margin = margin

    def forward(self, out):
        # Distance between positive pairs
        distance_positive = (out["anchor"].cpu() - out["positive"].cpu()).pow(2).sum(1)

        # Distance between negative pairs
        distance_negative = (out["anchor"].cpu() - out["negative"].cpu()).pow(2).sum(1)

        return F.relu(distance_positive - distance_negative + self.margin).mean()