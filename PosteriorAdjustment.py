"""
Function for using posterior adjustment to improve scoring
"""
#%% Setup
import torch
import pandas as pd

#%% Function
def posterior_adjustment(class_probabilities, scores, labels=None):
    """

    :param class_probabilities: The expect distribution of class scores. Should sum to 100
    :param scores             : The model output scores for each sample
    :return:
    """
    # Determine the differences between expected and actual
    predictions = scores.argmax(dim=1)
    classes, counts = predictions.unique(return_counts=True)

    # Order and normalize
    actual_probabilities = counts[classes.sort()[1]].float() / predictions.shape[0]

    # Store in a DataFrame and sort
    df = pd.DataFrame({ "class"           : classes.sort()[0]
                      , "expected_prob"   : class_probabilities
                      , "actual_prob"     : actual_probabilities
                      , "expected_classes": class_probabilities * predictions.shape[0]
                      , "difference"      : actual_probabilities - class_probabilities
                      }
                     ).sort_values("difference")

    indices = torch.arange(predictions.shape[0])
    results = []
    # Loop through each class
    for result in df.itertuples():
        # Get the vector that sorts according to current class
        sort_vec = scores[:,result[1]].sort(descending=True)[1]

        # Get the indices of the current class
        class_indices = indices[sort_vec][:int(result[4])]

        # Update the indices
        indices = indices[sort_vec][int(result[4]):]

        # Update the scores
        scores = scores[sort_vec][int(result[4]):]

        # Store results
        results.append(pd.DataFrame({ "index": class_indices
                                    , "class": result[1]
                                    }
                                   )
                      )

        # Check the accuracy of the class if labels are provided
        if labels is not None:
            unadjusted_class_accuracy = (predictions == labels)[labels == result[1]].float().mean()
            adjusted_class_accuracy   = (labels[class_indices] == result[1]).float().mean()

            print("Class {} | Baseline Accuracy: {}, Adjusted Accuracy: {}".format( result[1]
                                                                                  , unadjusted_class_accuracy
                                                                                  , adjusted_class_accuracy
                                                                                  )
                 )

    # Concatenate and sort
    results = torch.from_numpy(pd.concat(results).sort_values("index")["class"].values)

    return results

