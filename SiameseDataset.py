"""
Custom dataset class for siamese networks
"""

#%% Setup
import os
import torch
import torchvision.transforms.functional as FT

import numpy  as np
import pandas as pd

from PIL       import Image

#%% Dataset class
class KannadaMNIST(object):
    def __init__( self
                , image_folder    : str
                , labels          : pd.DataFrame
                , transforms      : list  = None
                ):
        """
        Class for creating a dataset for training the flow chart object detector
        """
        # Class attributes
        self.image_folder = image_folder
        self.labels       = labels
        self.transforms   = transforms

        # Create an index list
        self.index = np.arange(self.labels.shape[0])

    def __len__(self):
        return len(self.index)

    def __getitem__(self, idx):
        # Load the labels
        anchor_label = self.labels.iloc[self.index[idx]]

        # Load the image
        anchor_img = Image.open(self.image_folder + "/" + anchor_label["id"] + ".png")

        # Populate the labels
        anchor_label = anchor_label["label"]

        # Fetch a matching image
        positive_label = self.labels.query("label == {}".format(anchor_label)).sample(n=1)

        # Load the image
        positive_img = Image.open(self.image_folder + "/" + positive_label["id"].iloc[0] + ".png")

        # Fetch a non-matching image
        negative_label = self.labels.query("label != {}".format(anchor_label)).sample(n=1)

        # Load the image
        negative_img = Image.open(self.image_folder + "/" + negative_label["id"].iloc[0] + ".png")

        # Apply transformations
        anchor_img   = self.transforms(anchor_img)
        positive_img = self.transforms(positive_img)
        negative_img = self.transforms(negative_img)

        return {"anchor": anchor_img, "positive":positive_img, "negative":negative_img}
