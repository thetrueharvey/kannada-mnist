"""
Network implementation according to: https://www.kaggle.com/namanj27/11th-place-solution-code
"""
#%% Setup
from tensorflow import float32 as tf_float32

from tensorflow.keras.layers  import Input, Conv2D, MaxPooling2D, BatchNormalization, PReLU, Dropout, Flatten, Dense, Softmax
from tensorflow.keras         import Model

#%% Network function
def CustomNet(input_shape, n_class):
    # Create the input
    model_input = Input(shape=input_shape)

    # First section
    x = Conv2D(filters=64, kernel_size=3, padding="same")(model_input)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = Conv2D(filters=64, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = Conv2D(filters=64, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = MaxPooling2D(pool_size=2, strides=2)(x)

    # Second section
    x = Conv2D(filters=128, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = Conv2D(filters=128, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = Conv2D(filters=128, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = MaxPooling2D(pool_size=2, strides=2)(x)

    # Third section
    x = Conv2D(filters=256, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = Conv2D(filters=256, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = LeakyReLU(alpha=0.1)(x)
    x = PReLU(alpha_initializer="uniform")(x)

    x = MaxPooling2D(pool_size=2, strides=2)(x)

    # Fully connected layers
    x = Flatten()(x)

    x = Dense(units=256)(x)
    x = PReLU(alpha_initializer="uniform")(x)
    x = BatchNormalization()(x)

    x = Dense(units=256)(x)
    x = PReLU(alpha_initializer="uniform")(x)
    x = BatchNormalization()(x)

    x = Dense(units=n_class)(x)

    x = Softmax()(x)

    # Create a model
    model = Model( inputs =model_input
                 , outputs=x
                 )

    model.summary()

    return model
