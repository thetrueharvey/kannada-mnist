"""
Inception-style implementation of a custom classifier
"""
#%% Setup
from tensorflow.keras.layers  import Input, Conv2D, MaxPooling2D, BatchNormalization, PReLU, Dropout, Flatten, Dense, Concatenate
from tensorflow.keras         import Model, Sequential


#%% Function
def CustomNetV2(input_shape, n_class):
    # Create the input
    model_input = Input(shape=input_shape)

    # Network stem
    stem = Sequential([ conv_block(32)
                      , conv_block(64)
                      ]
                     )(model_input)

    # Inception cell 1
    x_1 = Conv2D(filters=int(stem.shape[-1] / 3), kernel_size=1)(stem)
    x_1 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_1)
    x_1 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_1)

    x_2 = Conv2D(filters=int(stem.shape[-1] / 2), kernel_size=1)(stem)
    x_2 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_2)
    x_2 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_2)

    x_2 = Conv2D(filters=int(x_1.shape[-1] * 2), kernel_size=1, padding="same")(x_2)
    x_2 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_2)
    x_2 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_2)

    x_3 = Conv2D(filters=int(stem.shape[-1] / 6), kernel_size=1)(stem)
    x_3 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_3)
    x_3 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_3)
    x_3 = Conv2D(filters=int(stem.shape[-1] / 3), kernel_size=1, padding="same")(x_3)
    x_3 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_3)
    x_3 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_3)
    x_3 = Conv2D(filters=int(stem.shape[-1] / 3), kernel_size=1, padding="same")(x_3)
    x_3 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_3)
    x_3 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_3)


    x_4 = MaxPooling2D(pool_size=3, strides=1, padding="same")(stem)
    x_4 = Conv2D(filters=int(stem.shape[-1] / 3), kernel_size=1)(x_4)
    x_4 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_4)
    x_4 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_4)

    x = Concatenate()([x_1, x_2, x_3, x_4, stem])
    x = MaxPooling2D(2, 2)(x)

    # Inception cell 2
    x_1 = Conv2D(filters=int(x.shape[-1] / 3), kernel_size=1)(x)
    x_1 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_1)
    x_1 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_1)

    x_2 = Conv2D(filters=int(x.shape[-1] / 2), kernel_size=1)(x)
    x_2 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_2)
    x_2 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_2)

    x_2 = Conv2D(filters=int(x_1.shape[-1] * 2), kernel_size=1, padding="same")(x_2)
    x_2 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_2)
    x_2 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_2)

    x_3 = Conv2D(filters=int(x.shape[-1] / 6), kernel_size=1)(x)
    x_3 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_3)
    x_3 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_3)
    x_3 = Conv2D(filters=int(x.shape[-1] / 3), kernel_size=1, padding="same")(x_3)
    x_3 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_3)
    x_3 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_3)
    x_3 = Conv2D(filters=int(x.shape[-1] / 3), kernel_size=1, padding="same")(x_3)
    x_3 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_3)
    x_3 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_3)

    x_4 = MaxPooling2D(pool_size=3, strides=1, padding="same")(x)
    x_4 = Conv2D(filters=int(x.shape[-1] / 3), kernel_size=1)(x_4)
    x_4 = PReLU(shared_axes=[1, 2], alpha_initializer="uniform")(x_4)
    x_4 = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x_4)

    x = Concatenate()([x_1, x_2, x_3, x_4])
    x = MaxPooling2D(2, 2)(x)
    x = Dropout(0.2)(x)

    # Fully connected layers
    x = Flatten()(x)

    x = Dense(units=512)(x)
    x = PReLU(alpha_initializer="uniform")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)
    #x = Dropout(0.2)(x)

    x = Dense(units=512)(x)
    x = PReLU(alpha_initializer="uniform")(x)
    x = BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")(x)

    x = Dense(units=n_class, activation="softmax")(x)

    # Create a model
    model = Model( inputs=model_input
                 , outputs=x
                 )

    model.summary()

    return model

def conv_block(filters):
    block = Sequential([ Conv2D(filters, 3, padding="same")
                       , PReLU(shared_axes=[1, 2], alpha_initializer="uniform")
                       , BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")
                       , Conv2D(filters, 3, padding="same")
                       , PReLU(shared_axes=[1, 2], alpha_initializer="uniform")
                       , BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")
                       , Conv2D(filters, 3, padding="same")
                       , PReLU(shared_axes=[1, 2], alpha_initializer="uniform")
                       , BatchNormalization(momentum=0.9, epsilon=1e-5, gamma_initializer="uniform")
                       ]
                      )

    return block

