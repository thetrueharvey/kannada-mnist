"""
Model dataset class
"""
#%% Setup
from random import uniform, randrange
import numpy as np

import matplotlib.pyplot as plt

from cv2 import imread, resize, IMREAD_GRAYSCALE

from tensorflow.keras.utils import Sequence
from tensorflow             import convert_to_tensor as to_T

#%% Dataset class
class Dataset(Sequence):
    def __init__( self
                , image_list
                , label_list
                , batch_size
                , dimensions
                , shuffle
                , transform = None
                ):
        """
        Creates a Keras Sequence class that serves data to the model
        """

        # Class attributes
        self.image_list = image_list
        self.label_list = label_list
        self.batch_size = batch_size
        self.dimensions = dimensions
        self.shuffle    = shuffle
        self.transform  = transform

        # Initialize the list
        self.on_epoch_end()

    def __len__(self):
        """
        Number of batches available in the dataset
        """
        return int(np.floor(len(self.image_list)) / self.batch_size)

    def __getitem__(self, index):
        """
        Generate a single batch of data
        """
        # Indices of samples in the dataset
        indices = self.indices[index * self.batch_size:(index + 1) * self.batch_size]

        # Associated images and labels
        images = [self.image_list[k] for k in indices]
        labels = [self.label_list[k] for k in indices]

        # Data generation
        X, y = self.__data_generation(images, labels)

        return (X, y)

    def __data_generation(self, images, labels):
        """
        Retrieve the appropriate image and process as necessary for training
        """
        # Empty storage
        X = np.empty((self.batch_size, self.dimensions["height"], self.dimensions["width"], self.dimensions["channels"]))
        y = []

        # Loop through each ID
        for idx, (image, label) in enumerate(zip(images, labels)):
            # Load the image
            image = imread(image, IMREAD_GRAYSCALE)

            # Load and resize
            image = resize( image
                          , (self.dimensions["width"], self.dimensions["height"])
                          )
            image = np.expand_dims(image, axis=2)

            # Erasing
            random_erasing(image)

            # Append to storage
            X[idx,] = image
            y.append(label)

        # Normalize X
        X = X / 255.0

        # Apply transformations
        if self.transform is not None:
            for X_,y_ in self.transform.flow(X,y, batch_size=self.batch_size, shuffle=False):
                X = X_
                y = y_
                break

        return to_T(X), to_T(y)

    def on_epoch_end(self):
        """
        Update the dataset at the end of a training epoch
        """
        self.indices = np.arange(len(self.image_list))

        # Shuffle if required
        if self.shuffle:
            np.random.shuffle(self.indices)


#%% Custom transforms
def random_erasing(img, h_range=[0.1,0.2], w_range=[0.1,0.2], probability=0.7):
    # Generate h and w values
    h = int(uniform(h_range[0], w_range[1]) * img.shape[0])
    w = int(uniform(h_range[0], w_range[1]) * img.shape[1])

    # Generate the x and y coordinates
    y = randrange(0, img.shape[0] - h)
    x = randrange(0, img.shape[1] - w)

    # Choose a fill colour
    fill = randrange(0, 255)

    # Update the image
    img[y:(y+h), x:(x+w)] = fill
