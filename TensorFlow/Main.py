"""
Main script for training a TensorFlow model
"""
#%% Setup
from glob import glob

import numpy  as np
import pandas as pd

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks           import ReduceLROnPlateau, EarlyStopping, TensorBoard, ModelCheckpoint
from tensorflow.keras.optimizers          import RMSprop, Nadam, Adadelta
from tensorflow.keras.models              import load_model

import Dataset
import CustomNet
import CustomNetV2

#%% Datasets
# Training
# Transforms
transform = ImageDataGenerator( rotation_range    =7
                              , zoom_range        =0.1
                              , width_shift_range =0.1
                              , height_shift_range=0.1
                              )

# Combine the datasets
df = pd.concat([pd.read_csv("dataset/train labels.csv"), pd.read_csv("dataset/test labels.csv")])
df = df.sample(frac=1.0, random_state=0)

# Create the training and testing datasets
train_df = df.iloc[:int(len(df) * 0.7)]
test_df  = df.iloc[int(len(df) * 0.7):]

train_image_list = ["dataset/combined/" + row["id"] + ".png" for i,row in train_df.iterrows()]

train_ds = Dataset.Dataset( image_list=train_image_list
                          , label_list=train_df["label"].values
                          , batch_size=256
                          , dimensions={"height":28, "width":28, "channels":1}
                          , shuffle   =True
                          , transform =transform
                          )

# Create the image list
test_image_list = ["dataset/combined/" + row["id"] + ".png" for i,row in test_df.iterrows()]

test_ds = Dataset.Dataset( image_list=test_image_list
                         , label_list=test_df["label"].values
                         , batch_size=512
                         , dimensions={"height":28, "width":28, "channels":1}
                         , shuffle   =False
                         )

#%% Network components
# Network
#net = CustomNet.CustomNet(input_shape=[28,28,1], n_class=10)
net = CustomNetV2.CustomNetV2(input_shape=[28,28,1], n_class=10)

# Optimizer
optimizer = RMSprop(learning_rate=0.02, rho=0.9)
#optimizer = Nadam()
#optimizer = Adadelta()

# Learning rate callback
learning_rate_reduction = ReduceLROnPlateau( monitor  ='loss'
                                           , factor   =0.2
                                           , patience =2
                                           , verbose  =1
                                           , mode     ="min"
                                           , min_delta=0.0001
                                           , cooldown =0
                                           , min_lr   =0.00001
                                           )

# Early stopping
early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=300, restore_best_weights=True)

# Tensorboard
tensorboard = TensorBoard(log_dir="tensorboard")

# Saving
checkpoint = ModelCheckpoint(filepath="checkpoints/CustomNet.hdf5", monitor="val_loss", mode="min", save_best_only=True)

#net = load_model("checkpoints/CustomNetV2.hdf5")

# Compile the network
net.compile(optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

#%% Training
#net = load_model("checkpoints/test.h5")
train_history = net.fit_generator( generator      =train_ds
                                 , validation_data=test_ds
                                 , epochs         =100
                                 , callbacks      =[learning_rate_reduction, early_stopping, checkpoint]
                                 )

# Save
a=0

'''
score = model.evaluate(np.array(xval), np.array(yval), batch_size=512)

# ytest=model.predict(xtest)
# ytest=np.argmax(ytest,axis=1)
# id_col=np.arange(ytest.shape[0])
# # print(id_col)

# sample=pd.read_csv(os.path.join(dirname,'sample_submission.csv'))
# sample['label']=ytest
# sample.to_csv('submission.csv',index=False)


#################
# USE IT TODAY

ytest = model.predict(xtest).argmax(axis=-1)
id_col = np.arange(ytest.shape[0])
submission = pd.DataFrame({'id': id_col, 'label': ytest})
submission.to_csv('submission.csv', index=False)

##################


# sub_preds = model.predict_classes(test_df)
# id_col = np.arange(sub_preds.shape[0])
# submission = pd.DataFrame({'id': id_col, 'label': ytest})
# submission.to_csv('submission.csv', index = False)

print("accuracy", score[1])
'''