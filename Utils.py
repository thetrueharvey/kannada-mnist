"""
Collection of utility functions for model training and building
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch
import torch.nn.functional as F

from sklearn.cluster import KMeans

#%% Function
def train( name
         , train_loader
         , test_loaders
         , net
         , loss_fn
         , optimizer
         , scheduler
         , logger
         , scheduler_type="per batch"
         , epoch_start   =1
         , epoch_end     =100
         , half          =False
         , early_stopping=10
         , start_accuracy=0
         ):
    """

    :param name:
    :param dataset:
    :param loader:
    :param net:
    :param loss_fn:
    :param optimizer:
    :param epoch_start:
    :param epoch_end:
    :param half:
    :param early_stopping:
    :return:
    """
    train_logger = logger()
    test_loggers = [logger() for _ in test_loaders]

    # Track the best accuracy and the epochs since testing accuracy improved
    best_acc = start_accuracy
    stationary_epochs = 0

    # Convert to half precision if desired
    if half:
        net.half()

    for epoch in range(epoch_start, epoch_end + 1):
        # Training
        net.train()
        for i, sample in enumerate(train_loader):
            optimizer.zero_grad()

            # Forward pass
            out = net(sample["image"].cuda())

            # Loss
            loss_ = loss_fn(out, sample["label"])

            # Back propogation
            loss_.backward()
            optimizer.step()

            # Scheduler update
            if scheduler_type == "per batch":
                scheduler.step()

            # Logger
            train_logger(out, sample["label"], epoch, loss_.detach().item())

            # Print a summary every 10 minibatches
            if (i + 1) % 10 == 0:
                print("Training Epoch: {} {}".format(epoch, train_logger.print_latest()))

        # Testing
        net.eval()
        with torch.no_grad():
            for i,loader in enumerate(test_loaders):
                for j, sample in enumerate(loader):
                    # Forward pass
                    out = net(sample["image"].cuda())

                    # Loss
                    loss_ = loss_fn(out, sample["label"])

                    # Logger
                    test_loggers[i](out, sample["label"], epoch, loss_.detach().item())

        # Update learning rate
        if scheduler_type == "per epoch":
            scheduler.step(test_loggers[0].accuracy[-1])

        for test_logger in test_loggers:
            print("Testing Epoch: {} {}".format(epoch, test_logger.print_latest()))

        # Cleanup
        torch.cuda.empty_cache()

        # Save a distinct checkpoint every 5 epochs
        torch.save({ "epoch": epoch
                   , "model_state_dict"    : net.state_dict()
                   , "optimizer_state_dict": optimizer.state_dict()
                   , "train_log"           : train_logger
                   , "test_log"            : test_loggers[0]
                   }
                  , "checkpoints/{} Weights.tar".format(name)
                  )

        if test_logger.accuracy[-1] > best_acc:
            best_acc = test_logger.accuracy[-1]

            torch.save({ "epoch": epoch
                       , "model_state_dict"    : net.state_dict()
                       , "optimizer_state_dict": optimizer.state_dict()
                       , "train_log"           : train_logger
                       , "test_log"            : test_logger
                       }
                      , "checkpoints/{} Best Weights.tar".format(name)
                      )

            stationary_epochs = 0
        else:
            stationary_epochs += 1

    return best_acc, epoch


def predict(loader, net, loss_fn=None):
    # Ensure that samples are not being shuffled
    loader.shuffle = False

    # Storage
    ids         = []
    predictions = []
    confidences = []
    losses      = []
    labels      = []

    # Iterate through the loader
    net.eval()
    with torch.no_grad():
        for i, sample in enumerate(loader):
            labels.append(sample["label"])
            ids += sample["id"]

            # Forward pass
            out = net(sample["image"].cuda())

            # Loss
            if loss_fn is not None:
                losses.append(loss_fn(out, sample["label"]).detach())

            # Predictions
            predictions.append(F.softmax(out["prediction"]))

            # Confidences
            confidences.append(out["confidence"])

    # Concatenate
    predictions = torch.cat(predictions)
    confidences = torch.cat(confidences)
    losses      = torch.cat(losses) if len(losses) > 0 else losses
    labels      = torch.cat(labels)

    return {"predictions":predictions, "confidences":confidences, "losses":losses, "labels":labels, "ids":np.array(ids)}

def pseudo_label(results, label_df):
    # Concatenate
    predictions = results["predictions"]
    confidences = results["confidences"]
    ids         = results["ids"]

    # Cluster to determine which labels are 'confident'
    kmeans = KMeans(n_clusters=2).fit(confidences)

    # Fetch the confident labels
    indices = kmeans.labels_ == np.argmin(kmeans.cluster_centers_)

    # Hence create a new DataFrame
    df = label_df[label_df["id"].isin(ids[indices])]
    df["label"] = predictions[label_df["id"].isin(ids[indices])]

    return df

def extend_dataset(results, p_hard=0.3, p_easy=0.05):
    # Indices
    correct_indices   = results["predictions"] == results["labels"]
    incorrect_indices = results["predictions"] != results["labels"]

    # Datasets
    correct_df = pd.DataFrame({ "id"   :results["ids"][correct_indices]
                              , "label":results["labels"][correct_indices]
                              , "loss" :results["losses"][correct_indices]
                              }
                             )

    incorrect_df = pd.DataFrame({ "id"   : results["ids"][incorrect_indices]
                                , "label": results["labels"][incorrect_indices]
                                , "loss" : results["losses"][incorrect_indices]
                                }
                               )

    # Filter and create a DataFrame
    label_df = pd.concat([ correct_df.sort_values(by="loss").iloc[int(len(correct_df) * p_easy):]
                         , incorrect_df.sort_values(by="loss").iloc[:int(len(correct_df) * p_hard)]
                         ]
                        )

    return label_df
